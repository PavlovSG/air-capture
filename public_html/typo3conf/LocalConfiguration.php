<?php
return [
    'BE' => [
        'debug' => true,
        'explicitADmode' => 'explicitAllow',
        'installToolPassword' => '$P$CLpx6A1zlr6XAg8yyz5.l7ekCLssYH.',
        'loginSecurityLevel' => 'normal',
        'passwordHashing' => [
            'className' => 'TYPO3\\CMS\\Core\\Crypto\\PasswordHashing\\PhpassPasswordHash',
            'options' => [],
        ],
    ],
    'DB' => [
        'Connections' => [
            'Default' => [
                'charset' => 'utf8mb4',
                'dbname' => 'typo3_dev',
                'driver' => 'mysqli',
                'host' => '172.17.0.1',
                'password' => 'dev',
                'port' => 3306,
                'tableoptions' => [
                    'charset' => 'utf8mb4',
                    'collate' => 'utf8mb4_unicode_ci',
                ],
                'user' => 'root',
            ],
        ],
    ],
    'EXT' => [
        'extConf' => [
            'aircapturevimeovideo' => 'a:1:{s:6:"vimeo.";a:3:{s:7:"client.";a:2:{s:10:"identifier";s:40:"ff8131fe8ab34b7582aab68eab35c5948cd738de";s:6:"secret";s:128:"OWR5Vh4I4tTxoJt2I8SCM8vouUou9VeJKBWmQgV7yE/tlMzgq1NyaULnKjvSijQFujmF0Bsoc5g1iKu6WmGK5qSqdhYW6qOJ9Wz9dfO0GNrfR3v/UMWa09obviwABlYY";}s:5:"token";s:32:"df5f19023a90159fd89a409c1ff2bbb8";s:7:"user_id";s:9:"107812240";}}',
            'backend' => 'a:6:{s:9:"loginLogo";s:0:"";s:19:"loginHighlightColor";s:0:"";s:20:"loginBackgroundImage";s:0:"";s:13:"loginFootnote";s:0:"";s:11:"backendLogo";s:0:"";s:14:"backendFavicon";s:0:"";}',
            'extension_builder' => 'a:3:{s:15:"enableRoundtrip";s:1:"1";s:15:"backupExtension";s:1:"1";s:9:"backupDir";s:35:"uploads/tx_extensionbuilder/backups";}',
            'extensionmanager' => 'a:2:{s:21:"automaticInstallation";s:1:"1";s:11:"offlineMode";s:1:"0";}',
            'gridelements' => 'a:6:{s:20:"additionalStylesheet";s:0:"";s:19:"nestingInListModule";s:1:"0";s:26:"overlayShortcutTranslation";s:1:"0";s:19:"disableDragInWizard";s:1:"0";s:25:"disableCopyFromPageButton";s:1:"0";s:38:"disableAutomaticUnusedColumnCorrection";s:1:"0";}',
            'lfeditor' => 'a:9:{s:13:"viewLanguages";s:0:"";s:15:"defaultLanguage";s:0:"";s:11:"searchRegex";s:60:"/^([a-z0-9_]*locallang[a-z0-9_-]*\\.(php|xml)|[^\\.]*\\.xlf)$/i";s:9:"extIgnore";s:23:"/^(CVS|.svn|.git|csh_)/";s:12:"extWhitelist";s:0:"";s:13:"changeXlfDate";s:1:"1";s:17:"editModeExtension";s:1:"1";s:27:"pathAdditionalConfiguration";s:37:"typo3conf/AdditionalConfiguration.php";s:16:"beMainModuleName";s:4:"user";}',
            'pb_social' => 'a:1:{s:11:"socialfeed.";a:11:{s:6:"devmod";s:1:"0";s:15:"ignoreVerifySSL";s:0:"";s:9:"facebook.";a:1:{s:4:"api.";a:3:{s:2:"id";s:0:"";s:6:"secret";s:0:"";s:15:"pageaccesstoken";s:0:"";}}s:10:"instagram.";a:1:{s:7:"client.";a:5:{s:2:"id";s:0:"";s:6:"secret";s:0:"";s:8:"callback";s:0:"";s:11:"access_code";s:0:"";s:12:"access_token";s:0:"";}}s:9:"linkedin.";a:2:{s:7:"client.";a:3:{s:3:"key";s:0:"";s:6:"secret";s:0:"";s:12:"callback_url";s:0:"";}s:12:"access_token";s:0:"";}s:10:"pinterest.";a:1:{s:4:"app.";a:3:{s:2:"id";s:0:"";s:6:"secret";s:0:"";s:4:"code";s:0:"";}}s:8:"twitter.";a:2:{s:9:"consumer.";a:2:{s:3:"key";s:0:"";s:6:"secret";s:0:"";}s:6:"oauth.";a:1:{s:7:"access.";a:2:{s:5:"token";s:0:"";s:12:"token_secret";s:0:"";}}}s:8:"youtube.";a:1:{s:6:"apikey";s:0:"";}s:6:"vimeo.";a:2:{s:7:"client.";a:2:{s:10:"identifier";s:0:"";s:6:"secret";s:0:"";}s:5:"token";s:0:"";}s:7:"tumblr.";a:3:{s:9:"consumer.";a:2:{s:3:"key";s:0:"";s:6:"secret";s:0:"";}s:5:"token";s:0:"";s:12:"token_secret";s:0:"";}s:6:"imgur.";a:1:{s:7:"client.";a:2:{s:2:"id";s:0:"";s:6:"secret";s:0:"";}}}}',
            'scheduler' => 'a:2:{s:11:"maxLifetime";s:4:"1440";s:15:"showSampleTasks";s:1:"1";}',
            'vhs' => 'a:1:{s:20:"disableAssetHandling";s:1:"0";}',
        ],
    ],
    'EXTENSIONS' => [
        'aircapturevimeovideo' => [
            'vimeo' => [
                'client' => [
                    'identifier' => 'ff8131fe8ab34b7582aab68eab35c5948cd738de',
                    'secret' => 'OWR5Vh4I4tTxoJt2I8SCM8vouUou9VeJKBWmQgV7yE/tlMzgq1NyaULnKjvSijQFujmF0Bsoc5g1iKu6WmGK5qSqdhYW6qOJ9Wz9dfO0GNrfR3v/UMWa09obviwABlYY',
                ],
                'token' => 'df5f19023a90159fd89a409c1ff2bbb8',
                'user_id' => '107812240',
            ],
        ],
        'backend' => [
            'backendFavicon' => '',
            'backendLogo' => '',
            'loginBackgroundImage' => '',
            'loginFootnote' => '',
            'loginHighlightColor' => '',
            'loginLogo' => '',
        ],
        'extension_builder' => [
            'backupDir' => 'uploads/tx_extensionbuilder/backups',
            'backupExtension' => '1',
            'enableRoundtrip' => '1',
        ],
        'extensionmanager' => [
            'automaticInstallation' => '1',
            'offlineMode' => '0',
        ],
        'gridelements' => [
            'additionalStylesheet' => '',
            'disableAutomaticUnusedColumnCorrection' => '0',
            'disableCopyFromPageButton' => '0',
            'disableDragInWizard' => '0',
            'nestingInListModule' => '0',
            'overlayShortcutTranslation' => '0',
        ],
        'lfeditor' => [
            'beMainModuleName' => 'user',
            'changeXlfDate' => '1',
            'defaultLanguage' => '',
            'editModeExtension' => '1',
            'extIgnore' => '/^(CVS|.svn|.git|csh_)/',
            'extWhitelist' => '',
            'pathAdditionalConfiguration' => 'typo3conf/AdditionalConfiguration.php',
            'searchRegex' => '/^([a-z0-9_]*locallang[a-z0-9_-]*\\.(php|xml)|[^\\.]*\\.xlf)$/i',
            'viewLanguages' => '',
        ],
        'pb_social' => [
            'socialfeed' => [
                'devmod' => '0',
                'facebook' => [
                    'api' => [
                        'id' => '',
                        'pageaccesstoken' => '',
                        'secret' => '',
                    ],
                ],
                'ignoreVerifySSL' => '',
                'imgur' => [
                    'client' => [
                        'id' => '',
                        'secret' => '',
                    ],
                ],
                'instagram' => [
                    'client' => [
                        'access_code' => '',
                        'access_token' => '',
                        'callback' => '',
                        'id' => '',
                        'secret' => '',
                    ],
                ],
                'linkedin' => [
                    'access_token' => '',
                    'client' => [
                        'callback_url' => '',
                        'key' => '',
                        'secret' => '',
                    ],
                ],
                'pinterest' => [
                    'app' => [
                        'code' => '',
                        'id' => '',
                        'secret' => '',
                    ],
                ],
                'tumblr' => [
                    'consumer' => [
                        'key' => '',
                        'secret' => '',
                    ],
                    'token' => '',
                    'token_secret' => '',
                ],
                'twitter' => [
                    'consumer' => [
                        'key' => '',
                        'secret' => '',
                    ],
                    'oauth' => [
                        'access' => [
                            'token' => '',
                            'token_secret' => '',
                        ],
                    ],
                ],
                'vimeo' => [
                    'client' => [
                        'identifier' => '',
                        'secret' => '',
                    ],
                    'token' => '',
                ],
                'youtube' => [
                    'apikey' => '',
                ],
            ],
        ],
        'scheduler' => [
            'maxLifetime' => '1440',
            'showSampleTasks' => '1',
        ],
        'vhs' => [
            'disableAssetHandling' => '0',
        ],
    ],
    'FE' => [
        'debug' => true,
        'loginSecurityLevel' => 'normal',
        'passwordHashing' => [
            'className' => 'TYPO3\\CMS\\Core\\Crypto\\PasswordHashing\\PhpassPasswordHash',
            'options' => [],
        ],
    ],
    'GFX' => [
        'processor' => 'GraphicsMagick',
        'processor_allowTemporaryMasksAsPng' => false,
        'processor_colorspace' => 'RGB',
        'processor_effects' => false,
        'processor_enabled' => true,
        'processor_path' => '/usr/bin/',
        'processor_path_lzw' => '/usr/bin/',
    ],
    'MAIL' => [
        'transport' => 'sendmail',
        'transport_sendmail_command' => '/usr/sbin/sendmail -t -i',
        'transport_smtp_encrypt' => '',
        'transport_smtp_password' => '',
        'transport_smtp_server' => '',
        'transport_smtp_username' => '',
    ],
    'SYS' => [
        'devIPmask' => '*',
        'displayErrors' => 1,
        'encryptionKey' => '4c0a43421f6d0f1ce3b42da17dc7fd8444208efa1e06177c3bdf500f2398bec460b4897879ffbc73b260cef16d40df71',
        'exceptionalErrors' => 12290,
        'features' => [
            'newTranslationServer' => true,
            'unifiedPageTranslationHandling' => true,
        ],
        'sitename' => 'Air-capture',
        'systemLogLevel' => 0,
        'systemMaintainers' => [
            1,
        ],
        'trustedHostsPattern' => '.*',
    ],
];
