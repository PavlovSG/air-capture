<?php
namespace T3Dev\Aircapturevimeovideo\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \Vimeo\Vimeo;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

class AircapturevimeovideoController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    protected $contentObj;

    public function initializeAction()
    {
    }

    /**
     * Load JS Libraries and Code
     */
    protected function loadResources()
    {
        /** @var \TYPO3\CMS\Core\Page\PageRenderer $pageRenderer */
        $pageRenderer = GeneralUtility::makeInstance( \TYPO3\CMS\Core\Page\PageRenderer::class);
        $extRelPath = ExtensionManagementUtility::siteRelPath('aircapturevimeovideo');
        $pageRenderer->addCssFile($extRelPath . "Resources/Public/Css/style.css", 'stylesheet', 'all', '', true);
    }

    /**
     * @return string The rendered view
     */
    public function indexAction()
    {
        $this->loadResources();
        $this->contentObj = $this->configurationManager->getContentObject();

        $data = $this->contentObj->data;
        #\TYPO3\CMS\Core\Utility\DebugUtility::debug($data);
        #$vimeo_url = $data['tx_aircapturevimeovideo_url'];

        #if(preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $vimeo_url, $output_array)) {
        #	$vimeo_id = $output_array[5];
        #}
        $vimeo_id = $data['tx_aircapturevimeovideo_id'];
        if ($vimeo_id=='0') {
            $videosOnServer=$this->getVideoList(1);
            $video=$videosOnServer[0];
            $vimeo_url = $video['link'];

            if (preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $vimeo_url, $output_array)) {
                $vimeo_id = $output_array[5];
            }
        }

        //\TYPO3\CMS\Core\Utility\DebugUtility::debug($vimeo_id);

        $caption = $data['tx_aircapturevimeovideo_caption'];
        $autoplay = $data['tx_aircapturevimeovideo_autoplay'];
        $loop = $data['tx_aircapturevimeovideo_loop'];
        $title = $data['tx_aircapturevimeovideo_title'];

        $ratio = $data['tx_aircapturevimeovideo_ratio'];

        $uid = $data['uid'];
        
        $this->view->assign('code', $vimeo_id);
        $this->view->assign('caption', $caption);
        $this->view->assign('autoplay', $autoplay);
        $this->view->assign('loop', $loop);
        $this->view->assign('title', $title);
        $this->view->assign('ratio', $ratio);

        $this->view->assign('uid', $uid);
        $this->view->assign('data', $data);
    }

    /**
     * @return string The rendered view
     */
    public function listAction()
    {
        $this->loadResources();
        $this->contentObj = $this->configurationManager->getContentObject();
        $data = $this->contentObj->data;
        $caption = $data['tx_aircapturevimeovideo_caption'];
        $autoplay = $data['tx_aircapturevimeovideo_autoplay'];
        $loop = $data['tx_aircapturevimeovideo_loop'];
        $title = $data['tx_aircapturevimeovideo_title'];
        $ratio = $data['tx_aircapturevimeovideo_ratio'];
        $listtype = $data['tx_aircapturevimeovideo_listtype'];
        $howMuchVideos = 12;
        if ($listtype == 14) {
            $howMuchVideos = 5;
        }
        $uid = $data['uid'];

        $videosOnServer=$this->getVideoList($howMuchVideos);
        $videoList=[];
        foreach ($videosOnServer as $video) {
            $vimeo_url = $video['link'];

            if (preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $vimeo_url, $output_array)) {
                $vimeo_id = $output_array[5];
            }
            $dataForm = [];

            //$dataForm['caption'] = $video['description'];
            $dataForm['autoplay'] = $autoplay;
            $dataForm['loop'] = $loop;
            $dataForm['title'] = $title;
            $dataForm['ratio'] = $ratio;
            $dataForm['code'] = $vimeo_id;
            $dataForm['caption'] = $video['name'];
            $dataForm['description'] = $video['description'];
            $pictures=$video['pictures']['sizes'];
            $picture=$pictures[count($pictures)-1];
            $dataForm['picture_link'] = $picture['link'];
            #\TYPO3\CMS\Core\Utility\DebugUtility::debug($video);
            $videoList[] = $dataForm;
        }


        /*        $videoList[1]=$dataForm;
                $videoList[2]=$dataForm;
                $videoList[3]=$dataForm;
                $videoList[4]=$dataForm;
                $videoList[5]=$dataForm;
                $videoList[6]=$dataForm;
                $videoList[7]=$dataForm;*/
        #\TYPO3\CMS\Core\Utility\DebugUtility::debug($data);
        $this->view->assign('videoList', $videoList);
        $this->view->assign('uid', $uid);
        $this->view->assign('listtype', $listtype);
    }

    /**
     * @return array
     */
    private function getVideoList($howMuchVideos=30)
    {
        $extConf = @unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['aircapturevimeovideo']);
        #\TYPO3\CMS\Core\Utility\DebugUtility::debug($extConf);
        if (isset($extConf['vimeo.']['client.']['identifier'])) {
            $client_id=$extConf['vimeo.']['client.']['identifier'];
        }
        if (isset($extConf['vimeo.']['client.']['secret'])) {
            $client_secret=$extConf['vimeo.']['client.']['secret'];
        }
        if (isset($extConf['vimeo.']['token'])) {
            $access_token=$extConf['vimeo.']['token'];
        }
        if (isset($extConf['vimeo.']['user_id'])) {
            $user_id=$extConf['vimeo.']['user_id'];
        }

        $client = new Vimeo($client_id, $client_secret, $access_token);
        $params=[];
        $params['page']=1;
        $params['per_page']=$howMuchVideos;
        $params['sort']='date';
        $params['direction']='desc';
        $response = $client->request('/users/'.$user_id.'/videos', $params, 'GET');
        #\TYPO3\CMS\Core\Utility\DebugUtility::debug($response);
        if ($response['status']==200) {
            return $response['body']['data'];
        }
        return [];
    }
}
