mod.wizards.newContentElement.wizardItems.common {
   elements {
      aircapturevimeovideo_pi1 {
         iconIdentifier = aircapturevimeovideo_icon
         title = Air-capture Vimeo video
         description = Vimeo video content element for Air Capture
         tt_content_defValues {
            CType = aircapturevimeovideo_pi1
         }
      }
      aircapturevimeovideo_pi2 {
         iconIdentifier = aircapturevimeovideo_icon
         title = Air-capture Vimeo video list
         description = Vimeo video list content element for Air Capture
         tt_content_defValues {
            CType = aircapturevimeovideo_pi2
         }
      }
   }
   show := addToList(aircapturevimeovideo_pi1)
   show := addToList(aircapturevimeovideo_pi2)
}
TCEFORM.tt_content.tx_aircapturevimeovideo_ratio.addItems {
	0 = Widescreen (16:9)
	1 = Standard (4:3)
}
TCEFORM.tt_content.tx_aircapturevimeovideo_listtype.addItems {
   3 = 3 column
   14 = 1+4 video
}
