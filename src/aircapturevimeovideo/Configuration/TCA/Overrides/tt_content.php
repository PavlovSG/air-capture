<?php
defined('TYPO3_MODE') || die('Access denied.');
    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['aircapturevimeovideo_pi1'] =  'aircapturevimeovideo_icon';
    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['aircapturevimeovideo_pi2'] =  'aircapturevimeovideo_icon';

    array_splice(
        $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'],
        6,
        0,
        array(
            array(
                'Air-capture Vimeo video',
                'aircapturevimeovideo_pi1',
                'aircapturevimeovideo_icon'
            ),
            array(
                'Air-capture Vimeo video list',
                'aircapturevimeovideo_pi2',
                'aircapturevimeovideo_icon'
            )
        )
    );

$tempColumns = array(
    'tx_aircapturevimeovideo_url' => array(
        'exclude' => 0,
        'label' => 'LLL:EXT:aircapturevimeovideo/Resources/Private/Language/locallang_db.xml:tx_aircapturevimeovideo_url.title',
        'config' => array(
            'type' => 'input',
            'size' => '50',
        ),
    ),
    'tx_aircapturevimeovideo_caption' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:aircapturevimeovideo/Resources/Private/Language/locallang_db.xml:tx_aircapturevimeovideo_caption.title',
        'config' => array(
            'type' => 'input',
            'size' => '50',
        ),
    ),
    'tx_aircapturevimeovideo_autoplay' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:aircapturevimeovideo/Resources/Private/Language/locallang_db.xml:tx_aircapturevimeovideo_autoplay.title',
        'config' => array(
            'type' => 'check',
            'items' => array(
                 array(
                     'LLL:EXT:lang/locallang_core.xlf:labels.enabled',
                 ),
            ),
        ),
    ),
    'tx_aircapturevimeovideo_loop' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:aircapturevimeovideo/Resources/Private/Language/locallang_db.xml:tx_aircapturevimeovideo_loop.title',
        'config' => array(
            'type' => 'check',
            'items' => array(
                 array(
                     'LLL:EXT:lang/locallang_core.xlf:labels.enabled',
                 ),
            ),
        ),
    ),
    'tx_aircapturevimeovideo_title' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:aircapturevimeovideo/Resources/Private/Language/locallang_db.xml:tx_aircapturevimeovideo_title.title',
        'config' => array(
            'type' => 'check',
            'items' => array(
                 array(
                     'LLL:EXT:lang/locallang_core.xlf:labels.enabled',
                 ),
            ),
        ),
    ),
    'tx_aircapturevimeovideo_ratio' => array(
        'exclude' => 1,
        'label'   => 'LLL:EXT:aircapturevimeovideo/Resources/Private/Language/locallang_db.xml:tx_aircapturevimeovideo_ratio.title',
        'config'  => array(
            'type'     => 'select',
            'items'    => array(), /* items set in page TsConfig */
            'size'     => 1,
            'maxitems' => 1
        )
    ),
    'tx_aircapturevimeovideo_id' => array(
        'exclude' => 0,
        'label' => 'LLL:EXT:aircapturevimeovideo/Resources/Private/Language/locallang_db.xml:tx_aircapturevimeovideo_id.title',
        'config' => array(
            'type' => 'input',
            'size' => '10',
            'eval' => 'required',
            'requiredCond' => '!field',
        ),
    ),
    'tx_aircapturevimeovideo_listtype' => array(
        'exclude' => 1,
        'label'   => 'LLL:EXT:aircapturevimeovideo/Resources/Private/Language/locallang_db.xml:tx_aircapturevimeovideo_listtype.title',
        'config'  => array(
            'type'     => 'select',
            'items'    => array(), /* items set in page TsConfig */
            'size'     => 1,
            'maxitems' => 1
        )
    ),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tt_content', 'EXT:aircapturevimeovideo/Resources/Private/Language/locallang_db.xml');


$GLOBALS['TCA']['tt_content']['types']['aircapturevimeovideo_pi1']['showitem'] = '

	--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
            --palette--;LLL:EXT:aircapturevimevideo/Resources/Private/Language/locallang_db.xml:tx_aircapturevimeovideo.title;aircapturevimeovideoMain1,
			--palette--;LLL:EXT:aircapturevimevideo/Resources/Private/Language/locallang_db.xml:tx_aircapturevimeovideo.settings;aircapturevimeovideoSettings,

        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
            --palette--;;language,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
            --palette--;;hidden,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
            categories,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
            rowDescription,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
		--div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements, tx_gridelements_container, tx_gridelements_columns
	';
$GLOBALS['TCA']['tt_content']['types']['aircapturevimeovideo_pi2']['showitem'] = '

	--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
            --palette--;LLL:EXT:aircapturevimevideo/Resources/Private/Language/locallang_db.xml:tx_aircapturevimeovideo.title;aircapturevimeovideoMain2,
			--palette--;LLL:EXT:aircapturevimevideo/Resources/Private/Language/locallang_db.xml:tx_aircapturevimeovideo.settings;aircapturevimeovideoSettings,

        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
            --palette--;;language,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
            --palette--;;hidden,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
            categories,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
            rowDescription,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
		--div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements, tx_gridelements_container, tx_gridelements_columns
	';


$GLOBALS['TCA']['tt_content']['palettes']['aircapturevimeovideoMain1']['showitem'] = '
	tx_aircapturevimeovideo_id,
	--linebreak--,
	tx_aircapturevimeovideo_caption';

$GLOBALS['TCA']['tt_content']['palettes']['aircapturevimeovideoMain2']['showitem'] = '
	tx_aircapturevimeovideo_listtype,
	--linebreak--,
	tx_aircapturevimeovideo_caption';

$GLOBALS['TCA']['tt_content']['palettes']['aircapturevimeovideoSettings']['showitem'] = '
	tx_aircapturevimeovideo_ratio,
	--linebreak--,
	tx_aircapturevimeovideo_autoplay,tx_aircapturevimeovideo_loop,tx_aircapturevimeovideo_title';

$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['aircapturevimeovideo_pi1'] =  'aircapturevimeovideo_icon';
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['aircapturevimeovideo_pi2'] =  'aircapturevimeovideo_icon';
