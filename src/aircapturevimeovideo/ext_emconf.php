<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "aircapturevimeovideo".
 *
 * Auto generated 11-04-2017 07:17
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
  'title' => 'Air-capture Vimeo Video',
  'description' => 'Easy to use Vimeo video for Air-capture.',
  'category' => 'fe',
  'version' => '0.0.1',
  'state' => 'alpha',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearcacheonload' => true,
  'author' => 'T3Dev',
  'author_email' => '',
  'author_company' => 'T3Dev',
  'constraints' =>
  array(
    'depends' =>
    array(
      'typo3' => '9.5.0-9.5.99',
      'fluid_styled_content' => '',
    ),
    'conflicts' =>
    array(
    ),
    'suggests' =>
    array(
    ),
  ),
);
