#
# Table structure for table 'backend_layout'
#
CREATE TABLE tt_content (
	tx_aircapturevimeovideo_id tinytext,
	tx_aircapturevimeovideo_caption tinytext,
	tx_aircapturevimeovideo_autoplay tinyint(1) unsigned DEFAULT '0' NOT NULL,
	tx_aircapturevimeovideo_loop tinyint(1) unsigned DEFAULT '0' NOT NULL,
	tx_aircapturevimeovideo_title tinyint(1) unsigned DEFAULT '0' NOT NULL,
	tx_aircapturevimeovideo_ratio tinyint(1) unsigned DEFAULT '0' NOT NULL,
	tx_aircapturevimeovideo_listtype tinyint(1) unsigned DEFAULT '3' NOT NULL

);