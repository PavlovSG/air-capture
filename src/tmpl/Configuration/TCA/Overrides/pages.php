<?php
defined('TYPO3_MODE') or die();


// Additional fields for page
$GLOBALS['TCA']['pages']['columns'] += array(

    'section_class' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:page.section_class',
        'config' => array(
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => array(
                array('LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:page.select.section_content', 'content'),
                array('LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:page.select.section_contact_content', 'contact_content'),
                array('LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:page.select.section_drone_content', 'drone_content'),
            ),
            'size' => 1,
            'minitems' => 0,
            'maxitems' => 1,
        ),
    ),

    'container_class' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:page.container_class',
        'config' => array(
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => array(
                array('LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:page.select.container_video_block', 'video_block'),
                array('LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:page.select.container_contact_block', 'contact_block'),
                array('LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:page.select.container_height_87', 'height_87vh'),
                array('LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:page.select.container_service_block', 'service_block'),
            ),
            'size' => 1,
            'minitems' => 0,
            'maxitems' => 1,
        ),
    ),

);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages',
    'section_class',
    '1',
    'after:title'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages',
    'container_class',
    '1',
    'after:section_class'
);
