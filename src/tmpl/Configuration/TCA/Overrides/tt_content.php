<?php
defined('TYPO3_MODE') or die();

// Add new fields to tt_content for common use of custom CTypes

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', [

        'teaser_link' => [
            'label' => 'LL:EXT:cms/locallang_ttc.xml:header_link',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
            ],
        ],

        'slides_to_show' => [
            'label' => 'LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:tt_content.slides_to_show',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ],
        ],

        'slides_to_scroll' => [
            'label' => 'LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:tt_content.slides_to_scroll',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ],
        ],

        'video_id' => [
            'label' => 'LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:tt_content.video_id',
            'config' => [
                'type' => 'input',
                'size' => 20,
            ],
        ],

        'video_caption' => [
            'label' => 'LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:tt_content.video_caption',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 5,
                'eval' => 'trim'
            ],
        ],


    ]
);

