<?php
defined('TYPO3_MODE') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'Second Slider',
        'ce_second_slider',
        'EXT:core/Resources/Public/Icons/T3Icons/content/content-carousel-image.svg'
    ),
    'CType',
    'tmpl'
);


$GLOBALS['TCA']['tt_content']['types']['ce_second_slider'] = [
    'showitem' => '
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.general;general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.header;header,
         bodytext;LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:ce_second_slider.text,
         slides_to_show;LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:tt_content.slides_to_show,
         slides_to_scroll;LLL:EXT:tmpl/Resources/Private/Language/locallang_db.xlf:tt_content.slides_to_scroll,
         
         
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.media,assets,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
        --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
        --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:tabs.access,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.visibility;visibility,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.access;access,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:tabs.extended'
];