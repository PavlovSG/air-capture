CREATE TABLE pages (
    section_class varchar(255) DEFAULT '' NOT NULL,
    container_class varchar(255) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
	slides_to_show int(11) unsigned DEFAULT '0' NOT NULL,
	slides_to_scroll int(11) unsigned DEFAULT '0' NOT NULL,
	teaser_link varchar(255) DEFAULT '' NOT NULL,
	video_id varchar(255) DEFAULT '' NOT NULL,
	video_caption varchar(255) DEFAULT '' NOT NULL,
);
